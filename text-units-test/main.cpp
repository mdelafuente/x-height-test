#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickWindow>

#include "fontmetrics.h"


int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);

  QQuickWindow::setTextRenderType(QQuickWindow::NativeTextRendering);
  QQmlApplicationEngine engine;

  // custom font metrics measurement
  // doesn't show up on autocomplete lol
  FontMetrics fontMetrics;
  engine.rootContext()->setContextProperty("fontMetrics", &fontMetrics);
  // ^^^^^^
  // to be clear, this is not using or overriding the native FontMetrics QML item,
  // because it doesn't provide the caps height. We need to talk to C++ (QFontMetrics)
  // to get more in depth stuff like this


  const QUrl url(u"qrc:/text-units-test/Test2.qml"_qs);
  QObject::connect(
    &engine,
    &QQmlApplicationEngine::objectCreationFailed,
    &app,
    []() { QCoreApplication::exit(-1); },
    Qt::QueuedConnection
  );
  engine.load(url);

  return app.exec();
}
