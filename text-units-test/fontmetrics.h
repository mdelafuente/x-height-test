#ifndef FONTMETRICS_H
#define FONTMETRICS_H

#include <QObject>
#include <QFont>
#include <QFontMetrics>

class FontMetrics: public QObject
{
  Q_OBJECT

public:
  explicit FontMetrics(QObject* parent = nullptr);

  // explicit
  // FontMetrics(QObject* parent = nullptr): QObject(parent) {}

  Q_INVOKABLE
  int getXHeight(const QFont &font) {
    QFontMetrics metrics(font);
    return metrics.xHeight();
  }

  Q_INVOKABLE
  int getCapHeight(const QFont &font) {
    QFontMetrics metrics(font);
    return metrics.capHeight();
  }

  Q_INVOKABLE
  int getDescent(const QFont &font) {
    QFontMetrics metrics(font);
    return metrics.descent();
  }
};

#endif // FONTMETRICS_H
