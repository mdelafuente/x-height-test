# potato
# potato
# potato

cmake_minimum_required(VERSION 3.16)

project(text-units-test VERSION 0.1 LANGUAGES CXX)

# version mgmt
set(QT_MIN_VERSION "6.4")
set(KF_MIN_VERSION "5.240.0")

set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 "${QT_MIN_VERSION}" REQUIRED COMPONENTS
  Quick
  QuickControls2
)

# find_package(KF6 "${KF_MIN_VERSION}" REQUIRED COMPONENTS
#   Kirigami
# )

qt_standard_project_setup()

qt_add_executable(apptext-units-test
    main.cpp
)

qt_add_qml_module(apptext-units-test
    URI text-units-test
    VERSION 1.0
    QML_FILES  Test2.qml
    SOURCES fontmetrics.h fontmetrics.cpp
)

# Qt for iOS sets MACOSX_BUNDLE_GUI_IDENTIFIER automatically since Qt 6.1.
# If you are developing for iOS or macOS you should consider setting an
# explicit, fixed bundle identifier manually though.
set_target_properties(apptext-units-test PROPERTIES
#    MACOSX_BUNDLE_GUI_IDENTIFIER com.example.apptext-units-test
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

target_link_libraries(apptext-units-test
    PRIVATE Qt6::Quick
)

include(GNUInstallDirs)
install(TARGETS apptext-units-test
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
)
