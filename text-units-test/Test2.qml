import QtQuick 2.15
import QtQuick.Controls 2.15

ApplicationWindow {
  id: root
  width: 640
  height: 480
  visible: true
  title: "patata"
  color: "black"

  Text {
    id: notoSample
    visible: false
    font.family: "Noto Sans"
    font.pixelSize: 999
  }

  Text {
    id: andadaSample
    visible: false
    font.family: "Andada Pro"
    font.pixelSize: 999
  }

  function getRatio(font) {
    let xHeight = fontMetrics.getXHeight(font)
    let capHeight = fontMetrics.getCapHeight(font)
    let ratio = (capHeight / xHeight).toFixed(2)

    return ratio
  }

  readonly property var notoRatio: getRatio(notoSample.font)
  readonly property var andadaRatio: getRatio(andadaSample.font)

  // untested on hidpi, but according to qt.io docs,
  // `px` are device independent like `dp` on android (at least on macOS)
  // see https://doc.qt.io/qt-6/scalability.html#high-dpi-scaling-on-macos-and-ios
  readonly property double baseXdp: 10

  Rectangle {
    anchors.fill: parent
    color: "#1aff0000"

    ScrollView {
      anchors.fill: parent

      Column {
        anchors.fill: parent
        spacing: 20

        Repeater {
          model: 8

          delegate: Row {
            id: delRow
            // spacing: 20 * Math.max(index, 1)
            spacing: 10
            padding: 20

            //
            // Andada Pro
            Item {
              width: children[1].width
              height: children[1].height

              function getSizeMultiplier() {
                return (
                  root.andadaRatio
                      * (index + 1)
                ).toFixed(2)
              }

              function getPxSize() {
                return (
                  root.baseXdp // base x height
                      * getSizeMultiplier()
                ).toFixed(2)
              }

              Rectangle {
                color: "#26ffffff"

                height: parent.getPxSize() / root.andadaRatio
                anchors.bottom: txt.baseline
                width: txt.width
              }

              Rectangle {
                color: "#26ffffff"

                height: (parent.getPxSize() / root.andadaRatio) / root.andadaRatio
                anchors.bottom: txt.baseline
                width: txt.width
              }

              Text {
                id: txt
                text: `Lorem Ipsum ${parent.getPxSize()}px`
                color: "white"
                font.pixelSize: parent.getPxSize()
                font.family: "Andada Pro"
                verticalAlignment: Text.AlignVCenter
                leftPadding: 0
                rightPadding: 0
              }
            }


            //
            // Noto Sans
            Item {
              width: children[1].width
              height: children[1].height

              function getSizeMultiplier() {
                return (
                  root.notoRatio
                      * (index + 1)
                ).toFixed(2)
              }

              function getPxSize() {
                return (
                  root.baseXdp // base x height
                      * getSizeMultiplier()
                ).toFixed(2)
              }

              Rectangle {
                color: "#26ffffff"

                height: parent.getPxSize() / root.notoRatio
                anchors.bottom: ntxt.baseline
                width: ntxt.width
              }

              Rectangle {
                color: "#26ffffff"

                height: (parent.getPxSize() / root.notoRatio) / root.notoRatio
                anchors.bottom: ntxt.baseline
                width: ntxt.width
              }

              Text {
                id: ntxt
                text: `Lorem Ipsum ${parent.getPxSize()}px`
                color: "white"
                font.pixelSize: parent.getPxSize()
                font.family: "Noto Sans"
                verticalAlignment: Text.AlignVCenter
                leftPadding: 0
                rightPadding: 0
              }
            } // noto sans ends here
          } // row del ends here
        }
      }
    }
  }
}
